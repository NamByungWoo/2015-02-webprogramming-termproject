var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var User = require('../models/User');

module.exports = function(passport) {
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});

	passport.use('local-login', new LocalStrategy({
		usernameField : 'email',
		passwordField : 'password',
		passReqToCallback : true
	}, function(req, email, password, done) {
		process.nextTick(function () {
			User.findOne({email: email}, function(err, user) {
				if (err) {
					return done(err);
				}

				if (!user) {
					return done(null, false, req.flash('danger', '이메일 혹은 비밀번호가 일치하지 않습니다.'));
				}
				
				if (user.password === null){
					return done(null, false, req.flash('danger', '페이스북 정보로 가입된 아이디 입니다. 페이스북 계정으로 로그인 후 비밀번호를 설정해 주세요.'));
				}

				if (!user.validatePassword(password)) {
					return done(null, false, req.flash('danger', '이메일 혹은 비밀번호가 일치하지 않습니다.'));
				}
        
				if(!user.signupVerify){
					return done(null, false, req.flash('danger', '이메일 인증을 먼저 해주세요.'));
				}
				
				return done(null, user, req.flash('success', '로그인되었습니다.'));
			});
		});
	}));

	passport.use(new FacebookStrategy({
		clientID : '1645435212389800',
		clientSecret : '3493e010f82af62a035ebb2d15cb6e5a',
		callbackURL : '/auth/facebook/callback',
		profileFields : ["emails", "displayName", "name", "photos"]
	}, function(token, refreshToken, profile, done) {
		console.log(profile);
		if(typeof profile.emails == 'undefined'){
			// 페이스북 이메일 인증이 안된경우
			// email값을 받지 못해 진행이 불가함
			return done(null);
		}else{
			var email = profile.emails[0].value;
			process.nextTick(function (){
				User.findOne({email: email}, function(err, user) {
					if (err) {
						return done(err);
					}else {
						if(!user){
							// 회원정보 없을 경우 - 페이스북으로 최초가입
							var newUser = new User({
								name: profile.displayName,
								email: profile.emails[0].value,
								signupVerify: true
							});
							newUser.facebook.id = profile.id;
							newUser.facebook.token = profile.token;
							newUser.facebook.photo = profile.photos[0].value;
							newUser.save(function(err){
								if (err) {
									console.log(err);
									return done(err);
								}
								return done(null, newUser);
							});
						}else{
							return done(null, user);
						}
					}
				});
			});
		}
	}));
};
