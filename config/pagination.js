//페이지 네이션을 위한 함수 정의
//currentPage : 이동할페이지
//allDataCount : 전체글 수
//option : option객체

module.exports = function(currentPage, allDataCount, option){	
	// 전체글 수와 option객체의 정보를 토대로 전체 페이지 수를 구한다.
	var totalPage = (allDataCount % option.limit > 0) ? (parseInt(allDataCount/option.limit)+1) : parseInt(allDataCount/option.limit);
	var startNum = parseInt(currentPage) - parseInt(option.pagingGroupNum);
	var endNum = parseInt(currentPage) + parseInt(option.pagingGroupNum);
	
	if(startNum < 1){
		var addNum = 1 - startNum;
		startNum = 1;
		endNum = endNum + addNum;
	}
	if(endNum > totalPage){
		var dropNum = endNum - totalPage;
		endNum = totalPage;
		startNum = startNum - dropNum;
		startNum = startNum < 0 ? 1 : startNum;
	}
	
	// 페이지(list)에 넘겨 주어야 할 실제 paination객체 생성
	var paginationObj = {
			allDataCount: allDataCount,
			currentPage: currentPage,
			firstPage: {
				cls: currentPage==1?"hidden":"",
				url: currentPage==1?"#":option.listURL + "/page/1"
			},
			prevPage: {
				cls: currentPage==1?"hidden":"",
				url: currentPage==1?"#":option.listURL + "/page/"+(parseInt(currentPage)-1)
			},
			nextPage: {
				cls: currentPage==totalPage || allDataCount==0 ? "hidden":"",
				url: currentPage==totalPage || allDataCount==0 ? "#":option.listURL + "/page/"+(parseInt(currentPage)+1)
			},
			lastPage: {
				cls: currentPage==totalPage || allDataCount==0 ? "hidden":"",
				url: currentPage==totalPage || allDataCount==0 ? "#":option.listURL + "/page/"+totalPage
			}	
	};
	var pages = [];
	
	for(var i=0; i+startNum<=endNum; i++){
		pages[i] = {cls: currentPage==(i+startNum)?"active":"", url: option.listURL + "/page/"+(i+startNum), text: i+startNum};
	}
	paginationObj.pages = pages;
	
	return paginationObj;
};