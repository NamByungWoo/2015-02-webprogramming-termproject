var path = require('path');
var nodemailer = require('nodemailer');
var EmailTemplate = require('email-templates-v2').EmailTemplate;
var templatesDir = path.resolve(__dirname, '..', 'views/email_templates');

// google SMTP 사용
var transporter = nodemailer.createTransport({
	service: 'Gmail',
	auth: {
		user: 'mju.star.survey@gmail.com',	// 실제 gmail계정 새로 생성되어 있음
		pass: 'mjutermproject'
	}
});

// 폼전송 입력 내용과 mode(회원가입/비밀번호재설정)를 받아온다
module.exports = function(formObj, mode){	
	// 메일 템플릿 설정
	var template = new EmailTemplate(path.join(templatesDir, mode));
	template.render(formObj, function (err, results){
		if(err){
			return console.error(err);
		}
		// 실제 mail 전송
		transporter.sendMail(
			{
				from: (typeof formObj.email_from == 'undefined') ? 'Star Survey<noreply.mju.star.survey@gmail.com>' : '<' + formObj.email_from + '>',
				to: formObj.email,
				subject: results.subject,
				html: results.html,
				text: results.text
			}, 
			function(error, info){
				if(error){
					return console.log(error);
				}
				console.log('Message sent: ' + info.response);
			}
		);
	});
};