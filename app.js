var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var methodOverride = require('method-override');
var flash = require('connect-flash');
var mongoose   = require('mongoose');
var passport = require('passport');
var configAuth = require('./config/auth');

var routes = require('./routes/index');
var admin = require('./routes/admin');
var users = require('./routes/users');
var survey = require('./routes/survey');
var routeAuth = require('./routes/auth');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
if (app.get('env') === 'development') {
	app.locals.pretty = true;
}
app.locals.moment = require('moment');

// mongodb connect
// 서버내부 mongodb사용, id: admin, pw:mjutermproject 
mongoose.connect('mongodb://admin:mjutermproject@localhost:27017/starSurvey');
mongoose.connection.on('error', console.log);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(methodOverride('_method', {methods: ['POST', 'GET']}));

app.use(session({
	resave: true,
	saveUninitialized: true,
	secret: 'long-long-long-secret-string-1313513tefgwdsvbjkvasd'
}));
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components',  express.static(path.join(__dirname, '/bower_components')));

app.use(passport.initialize());
app.use(passport.session());

app.use(function(req, res, next) {
	res.locals.currentUser = req.user;
	res.locals.flashMessages = req.flash();
	next();
});

configAuth(passport);

app.use('/', routes);
app.use('/admin', admin);
app.use('/users', users);
app.use('/survey', survey);
routeAuth(app, passport);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if(app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		req.flash('danger', '에러입니다. 관리자에게 문의해 주세요.');
		res.redirect('/');
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	req.flash('danger', '에러입니다. 관리자에게 문의해 주세요.');
	res.redirect('/');
});

module.exports = app;