// 각 설문의 질문중 객관식 전용 테이블
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;

var schema = new Schema({
	surveyQuestion_id: {type: String, required: true, trim: true},
	multipleKind: {type: String, required: true},
	multipleName: {type: String},
	createdAt: {type: Date, default: Date.now}
},{
	toJSON: {virtuals: true},
	toObject: {virtuals: true}
});

var SurveyQuestionMultiple = mongoose.model('SurveyQuestionMultiple', schema);

module.exports = SurveyQuestionMultiple;