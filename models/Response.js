// 설문 테이블
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;

var schema = new Schema({
	survey_id: {type: String},
	survey_version: {type: Number},
	answer: {type: Object},
	createdAt: {type: Date, default: Date.now}
},{
	toJSON: {virtuals: true},
	toObject: {virtuals: true}
});

var Response = mongoose.model('Response', schema);

module.exports = Response;