// 설문 테이블
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;

var schema = new Schema({
	title: {type: String, required: true, trim: true},
	explain: {type: String, trim: true},
	useDate: {type: Boolean},
	startDate: {type: Date},
	endDate: {type: Date},
	question: {type: Object},
	updateVersion: {type: Number, default:0},
	userID: {type: String},
	createdAt: {type: Date, default: Date.now}
},{
	toJSON: {virtuals: true},
	toObject: {virtuals: true}
});

var Survey = mongoose.model('Survey', schema);

module.exports = Survey;