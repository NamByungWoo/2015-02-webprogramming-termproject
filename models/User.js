var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;

var schema = new Schema({
	name: {type: String, required: true, trim: true},
	email: {type: String, required: true, index: true, unique: true, trim: true},
	password: {type: String},
	signupVerify: {type: Boolean, required: true, default: false},
	createdAt: {type: Date, default: Date.now},
	isAdmin: {type:Boolean, required: true, default: false},
	facebook: {id: String, token: String, photo: String}
},{
	toJSON: {virtuals: true},
	toObject: {virtuals: true}
});

schema.methods.generateHash = function(password) {
	var salt = bcrypt.genSaltSync(10);
	return bcrypt.hashSync(password, salt);
};

schema.methods.validatePassword = function(password) {
	return bcrypt.compareSync(password, this.password);
};

var User = mongoose.model('User', schema);

module.exports = User;