// 각 설문의 질문 테이블
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;

var schema = new Schema({
	survey_id: {type: String, required: true, trim: true},
	questionName: {type: String, required: true, trim: true},
	questionKind: {type: String, required: true},
	createdAt: {type: Date, default: Date.now}
},{
	toJSON: {virtuals: true},
	toObject: {virtuals: true}
});

var SurveyQuestion = mongoose.model('SurveyQuestion', schema);

module.exports = SurveyQuestion;