$(document).ready(function(){
	
	// 설문기간 사용vs미사용
	$("#useDate").change(function(){
		if($(this).is(":checked")){
			$(".questionDate").removeAttr("readonly");
		}else{
			$(".questionDate").attr("readonly", "readonly");
		}
	});
	
	// 질문 삭제
	$(document).on("click", ".question-close", function(){
		var questionIdx = $(this).attr("data-question-idx");
		$("#question"+questionIdx).remove();
	});
	
	// 새로운 질문 추가
	$("#addQuestion").click(function(){
		var questionIdx = parseInt($(".question").last().attr("data-question-idx")) + 1;
		questionIdx = isNaN(questionIdx) ? 1 : questionIdx;
		
		var htmlString = '';
		htmlString += '<div id="question' + questionIdx + '" data-question-idx="' + questionIdx + '" class="form-group panel panel-default question">';
		
		htmlString += '<div class="panel-heading">';
		htmlString += '<h4 class="panel-title inline">질문 설정<input name="questionIdx" type="hidden" value="' + questionIdx + '")</h4>';
		htmlString += '<button type="button" data-question-idx="' + questionIdx + '" class="close question-close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>';
		htmlString += '</div>';
		htmlString += '<div class="panel-body">';
		htmlString += '<div class="form-group row">';
		htmlString += '<div class="col-md-12">';
		htmlString += '<input type="checkbox" id="questionIndispensable'+questionIdx+'" class="init-input-style questionIndispensable"/>';
		htmlString += '<input type="hidden" name="questionIndispensable" value="false">';
		htmlString += '<label for="questionIndispensable'+questionIdx+'">필수 질문</label>';
		htmlString += '</div>';
		htmlString += '</div>';
		htmlString += '<div class="form-group row">';
		htmlString += '<div class="col-md-2">';
		htmlString += '<label for="questionName">질문 제목</label>';
		htmlString += '</div>';
		htmlString += '<div class="col-md-10">';
		htmlString += '<input name="questionName" value="제목없는 질문" class="form-control"/>';
		htmlString += '</div>';
		htmlString += '</div>';
		htmlString += '<div class="form-group row">';
		htmlString += '<div class="col-md-2">';
		htmlString += '<label for="questionKind">질문 유형</label>';
		htmlString += '</div>';
		htmlString += '<div class="col-md-10">';
		htmlString += '<select name="questionKind" data-style="btn-primary" class="form-control questionKind" data-question-idx="' + questionIdx + '">';
		htmlString += '<option value="1" selected="selected">객관식</option>';
		htmlString += '<option value="2">단일입력(1줄 text)</option>';
		htmlString += '<option value="3">의견(long text)</option>';
		htmlString += '<option value="4">날짜</option>';
		htmlString += '</select>';
		htmlString += '</div>';
		htmlString += '</div>';
		htmlString += '<div class="form-group row">';
		htmlString += '<div id="question' + questionIdx + '_detail" class="col-md-12 question-detail">';
		htmlString += questionDetailHtml.kind1(questionIdx);
		htmlString += '</div>';
		htmlString += '</div>';

		$(this).parent().before(htmlString);
	});
	
	// 필수 질문 checkbox 제어
	$(document).on("change", ".questionIndispensable", function(){
		$(this).siblings("input[name=questionIndispensable]").val($(this).is(":checked"));
	});
	
	// 질문 유형 변경
	$(document).on("change", ".questionKind", function(){
		var questionIdx = $(this).attr("data-question-idx");
		$("#question" + questionIdx + "_detail").html(questionDetailHtml["kind"+$(this).val()](questionIdx));
	});
	
	// 객관식 선택 항목 추가
	$(document).on("click", ".question_multiple_add_btn", function(){
		var questionIdx = parseInt($(this).closest(".question").attr("data-question-idx"));
		var multipleIdx = parseInt($("#question" + questionIdx + "_detail").find(".real-input").last().attr("data-multiple-idx"));
		if($("#question" + questionIdx + "_detail").find(".real-input").length == 1){
			var deleteBtnHtml = '<span class="fa-stack fa-lg question_multiple_delete_btn" data-question-idx="' + questionIdx + '" data-multiple-idx="' + multipleIdx + '"><i class="fa fa-circle fa-stack-2x cursor-pointer hidden"></i><i class="fa fa-times fa-stack-1x focus-inverse"></i></span>';
			$("#question" + questionIdx + "_detail").children(".form-group").first().find("input").after(deleteBtnHtml);
		}
		multipleIdx++;
		
		var htmlString = '';
		htmlString += '<div id="question' + questionIdx + '_multiple' + multipleIdx + '" data-multiple-idx="' + multipleIdx + '" class="form-group row real-input">';
		htmlString += '<div class="col-md-3"></div>';
		htmlString += '<div class="col-md-7">';
		htmlString += '<input class="form-control form-control-70" name="question' + questionIdx + '_multiple" value="선택지" />';
		htmlString += '<span class="fa-stack fa-lg question_multiple_delete_btn" data-question-idx="' + questionIdx + '" data-multiple-idx="' + multipleIdx + '"><i class="fa fa-circle fa-stack-2x cursor-pointer hidden"></i><i class="fa fa-times fa-stack-1x focus-inverse"></i></span>';
		htmlString += '</div>';
		htmlString += '<div class="col-md-2"></div>';
		htmlString += '</div>';

		$(this).closest(".row").before(htmlString);
	});
	
	// 객관식 선택 항목 삭제
	$(document).on("click", ".question_multiple_delete_btn", function(){
		var questionIdx = $(this).attr("data-question-idx");
		var multipleIdx = $(this).attr("data-multiple-idx");
		$("#question" + questionIdx + "_multiple" + multipleIdx).remove();
		
		// 한개일 경우 삭제버튼 제거
		if($("#question" + questionIdx + "_detail").find(".real-input").length == 1){
			$("#question" + questionIdx + "_detail").find(".real-input").find(".question_multiple_delete_btn").remove();
		}
	});
	
	// 객관식 - 기타 추가
	$(document).on("click", ".etc-btn a", function(){
		var questionIdx = $(this).attr("data-question-idx");
		$("#question" + questionIdx + "_detail .etc-btn").addClass("hidden");
		$("#question" + questionIdx + "_detail .etc-input").removeClass("hidden");
		$("#question" + questionIdx + "_detail .etc-input").find("input").attr("name", "question" + questionIdx + "_multiple_etc");
	});
	
	// 객관식 - 기타 제거
	$(document).on("click", ".question_etc_delete_btn", function(){
		var questionIdx = $(this).attr("data-question-idx");
		$("#question" + questionIdx + "_detail .etc-btn").removeClass("hidden");
		$("#question" + questionIdx + "_detail .etc-input").addClass("hidden");
		$("#question" + questionIdx + "_detail .etc-input").find("input").attr("name", "");
	});
	
	$(".surveyDeleteBtn").click(function(e){
		var id = $(e.currentTarget).data('id');
		if(confirm('정말 삭제하시겠습니까?')){
			$("#surveyDeleteForm").attr('action', "/survey/" + id + "?_method=DELETE").submit();          
		}
		return false;
	});
});

var questionDetailHtml = {
		kind1 :
			function(questionIdx){
				var html = '';
				html += '<div id="question' + questionIdx + '_multiple1" data-multiple-idx="1" class="form-group row real-input">';
				html += '<div class="col-md-3"></div>';
				html += '<div class="col-md-7">';
				html += '<input name="question' + questionIdx + '_multiple" value="선택지" class="form-control form-control-70"/>';
				html += '</div>';
				html += '<div class="col-md-2"></div>';
				html += '</div>';
				html += '<div class="form-group row">';
				html += '<div class="col-md-3"></div>';
				html += '<div class="col-md-7">';
				html += '<input value="선택지를 추가하려면 클릭" readonly="readonly" class="form-control form-control-70 cursor-pointer question_multiple_add_btn"/><span class="etc-btn"> 또는 <a class="cursor-pointer" data-question-idx="' + questionIdx + '">\'기타\' 추가</a></span>';
				html += '</div>';
				html += '<div class="col-md-2"></div>';
				html += '</div>';
				html += '<div class="form-group row hidden etc-input">';
				html += '<div class="col-md-3"></div>';
				html += '<div class="col-md-7">';
				html += '<input name="" value="기타 : (답변)" readonly="readonly" class="form-control form-control-70"/>';
				html += '<span data-question-idx="' + questionIdx + '" class="fa-stack fa-lg question_etc_delete_btn"><i class="fa fa-circle fa-stack-2x cursor-pointer hidden"></i><i class="fa fa-times fa-stack-1x focus-inverse"></i></span>';		
				html += '</div>';
				html += '<div class="col-md-2"></div>';
				html += '</div>';
				return html;
			},
		kind2 :
			function(questionIdx){
				var html = '';
				html += '<div class="form-group row">';
				html += '<div class="col-md-3"></div>';
				html += '<div class="col-md-7">';
				html += '<input value="답변" readonly="readonly" class="form-control form-control-70 dashed"/>';
				html += '</div>';
				html += '<div class="col-md-2"></div>';
				html += '</div>';
				return html;
			},
		kind3 :
			function(questionIdx){
				var html = '';
				html += '<div class="form-group row">';
				html += '<div class="col-md-3"></div>';
				html += '<div class="col-md-7">';
				html += '<textarea readonly="readonly" class="form-control dashed">자세한 답변</textarea>';
				html += '</div>';
				html += '<div class="col-md-2"></div>';
				html += '</div>';
				return html;
			},
		kind4 :
			function(questionIdx){
				var html = '';
				html += '<div class="form-group row">';
				html += '<div class="col-md-3"></div>';
				html += '<div class="col-md-7">';
				html += '<input type="date" min="1900-01-01" max="2099-12-31" step=1 value="2015-01-01" readonly="readonly" class="form-control form-control-70 dashed">';
				html += '</div>';
				html += '<div class="col-md-2"></div>';
				html += '</div>';
				return html;
			}
};