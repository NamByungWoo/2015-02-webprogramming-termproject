$(document).ready(function(){
	// 회원삭제
	$(".userDeleteBtn").click(function(e){
		if(confirm('정말 탈퇴 하시겠습니까?')){
			$("#userDeleteForm").attr('action', "/users/?_method=DELETE").submit();          
		}
		return false;
	});
});