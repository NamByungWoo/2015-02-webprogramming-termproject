$(function(){
	$('table.highchart').highchartTable();
	
	$(".customTooltip").tooltip();
	
	// Font-Awesome 버튼용 hover
	$(document).on("mouseover", "i.focus-inverse", function(){
		$(this).addClass("fa-inverse");
		$(this).addClass("cursor-pointer");
		$(this).siblings().removeClass("hidden");
	});
	$(document).on("mouseleave", "i.focus-inverse", function(){
		$(this).removeClass("fa-inverse");
		$(this).removeClass("cursor-pointer");
		$(this).siblings().addClass("hidden");
	});	
	
	// 비밀번호 재설정 요청 email modal
	$("#forgot-password").click(function(){
		$("#loginModal").modal("hide");
		$("#forgotPasswordModal").modal();
	});
});