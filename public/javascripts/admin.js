$(document).ready(function(){
	// 회원삭제
	$(".userDeleteBtn").click(function(e){
		var id = $(e.currentTarget).data('id');
		if(confirm('정말 삭제하시겠습니까?')){
			$("#userDeleteForm").attr('action', "/admin/userDelete/" + id + "?_method=DELETE").submit();          
		}
		return false;
	});
});