$(function(){
	// validate필요한 form submit 요청시
	$(".useValidate").click(function(){
		var mode;
		mode = $(this).attr("data-validate-mode");
		validateFunctionObj[mode]();
	});
});

var validateFunctionObj = {
	signup :
		// 회원가입 validate
		function(){
			// validate 알림 초기화
			$("#signupAlert").addClass("hidden");
			$(".useValidate").parent().parent().find("input.form-control").each(function(){
				$(this).removeClass("focus");
			});
			
			$.ajax({
				url: "/users/signupAjax",
				data: $("#signupForm").serialize(),
				type: "post",
				dataType: "json",
				async: false,
				success: function(result){
					if(result.status){
						// 성공, submit실행
						$("#signupForm").submit();
					}else{
						$("#signupAlert").find("span").html(result.msg);
						$("#signupAlert").removeClass("hidden");
						$("#signupForm").find("#"+result.focus).addClass("focus");
					}
				}
			});
		},
	surveyInvite :
		// 설문 email 초대 validate
		function(){
			// validate 알림 초기화
			$("#signupAlert").addClass("hidden");
			$(".useValidate").parent().find("input").each(function(){
				$(this).removeClass("focus");
			});
			
			$.ajax({
				url: "/survey/inviteAjax",
				data: {email: $("#inviteModal").find("#email").val(), survey_id: $("#inviteBtn").data("id")},
				type: "post",
				dataType: "json",
				async: false,
				success: function(result){
					if(result.status){
						// 성공, modal close, alert open
						alert(result.msg);
						$('#inviteModal').modal('hide');
					}else{
						$("#inviteAlert").find("span").html(result.msg);
						$("#inviteAlert").removeClass("hidden");
						$("#inviteModal").find("#"+result.focus).addClass("focus");
					}
				}
			});
		},
	adduser :
		// 회원추가 validate
		function(){
			// validate 알림 초기화
			$("#adduesrAlert").addClass("hidden");
			$(".useValidate").parent().parent().find("input.form-control").each(function(){
				$(this).removeClass("focus");
			});
			
			$.ajax({
				url: "/admin/adduserAjax",
				data: $("#adduserForm").serialize(),
				type: "post",
				dataType: "json",
				async: false,
				success: function(result){
					if(result.status){
						// 성공, submit실행
						$("#adduserForm").submit();
					}else{
						$("#adduserAlert").find("span").html(result.msg);
						$("#adduserAlert").removeClass("hidden");
						$("#adduserForm").find("#"+result.focus).addClass("focus");
					}
				}
			});
		},
	mypage :
		// mypage(회원정보수정) validate
		function(){
			// validate 알림 초기화
			$("#mypageAlert").addClass("hidden");
			$(".useValidate").parent().parent().find("input.form-control").each(function(){
				$(this).removeClass("focus");
			});
			
			$.ajax({
				url: "/users/mypageAjax",
				data: $("#mypageForm").serialize(),
				type: "post",
				dataType: "json",
				async: false,
				success: function(result){
					if(result.status){
						// 성공, submit실행
						$("#mypageForm").submit();
					}else{
						$("#mypageAlert").find("span").html(result.msg);
						$("#mypageAlert").removeClass("hidden");
						$("#mypageForm").find("#"+result.focus).addClass("focus");
					}
				}
			});
		},
	useredit :
		// admin 회원정보수정 validate
		function(){
			// validate 알림 초기화
			$("#usereditAlert").addClass("hidden");
			$(".useValidate").parent().parent().find("input.form-control").each(function(){
				$(this).removeClass("focus");
			});
			
			$.ajax({
				url: "/admin/usereditAjax",
				data: $("#usereditForm").serialize(),
				type: "post",
				dataType: "json",
				async: false,
				success: function(result){
					if(result.status){
						// 성공, submit실행
						$("#usereditForm").submit();
					}else{
						$("#usereditAlert").find("span").html(result.msg);
						$("#usereditAlert").removeClass("hidden");
						$("#usereditForm").find("#"+result.focus).addClass("focus");
					}
				}
			});
		},
	forgotPassword :
		// 비밀번호 재설정 메일 전송
		function(){
			// validate 알림 초기화
			$("#forgotPasswordAlert").addClass("hidden");
			$(".useValidate").parent().parent().find("input.form-control").each(function(){
				$(this).removeClass("focus");
			});
			
			$.ajax({
				url: "/users/forgotPasswordAjax",
				data: $("#forgotPasswordForm").serialize(),
				type: "post",
				dataType: "json",
				async: false,
				success: function(result){
					if(result.status){
						// 성공, submit실행
						$("#forgotPasswordForm").submit();
					}else{
						$("#forgotPasswordAlert").find("span").html(result.msg);
						$("#forgotPasswordAlert").removeClass("hidden");
						$("#forgotPasswordForm").find("#"+result.focus).addClass("focus");
					}
				}
			});
		},
	resetPassword :
		// 비밀번호 재설정
		function(){
			// validate 알림 초기화
			$("#resetPasswordAlert").addClass("hidden");
			$(".useValidate").parent().parent().find("input.form-control").each(function(){
				$(this).removeClass("focus");
			});
			
			$.ajax({
				url: "/users/resetPasswordAjax",
				data: $("#resetPasswordForm").serialize(),
				type: "post",
				dataType: "json",
				async: false,
				success: function(result){
					if(result.status){
						// 성공, submit실행
						$("#resetPasswordForm").submit();
					}else{
						$("#resetPasswordAlert").find("span").html(result.msg);
						$("#resetPasswordAlert").removeClass("hidden");
						$("#resetPasswordForm").find("#"+result.focus).addClass("focus");
					}
				}
			});
		}
};