var express = require('express');
var router = express.Router();
var moment = require('moment');
var nodemailer = require('../config/nodemailer');
var pagination = require('../config/pagination');
var Survey = require('../models/Survey');
var Response = require('../models/Response');


function needAuth(req, res, next) {
	if(req.isAuthenticated()){
		next();
	}else{
		req.flash('danger', '로그인이 필요합니다.');
		res.redirect('/');
	}
}

// 페이지정보가 없을 때 무조건 1페이지로
router.get('/list', function(req, res, next) {
	res.redirect('/survey/list/page/1');
});

/* 설문 목록 페이지 */
router.get('/list/page/:page', needAuth, function(req, res, next) {
	Survey.count({userID: req.user.id}, function(err, count){
		// 페이징을 위한 db select option정의
		var option = {
			listURL: "/survey/list",
			limit: 10,	// 한번에 가져올 최대 데이터 수
			skip: (req.params.page-1)*10,	// 몇개의 데이터를 건너 뛰고 select할지...(현재 page에따라 변함)
			pagingGroupNum: 4,
			sort: {createdAt:-1}	// 등록일을 기준으로 내림차순 정렬
		};
		
		// 앞서 입력된 정보를 가지고 현재페이지에 나타낼 실제 글 목록을 select
		Survey.find({userID: req.user.id}, {}, option, function(err, surveys){
			if(err){
				return next(err);
			}else{
				res.render('survey/list', {surveys: surveys, pagination: pagination(req.params.page, count, option)});
			}
		});
	});
});

/* 설문 초대 validate + ajax 전송 */
router.post('/inviteAjax', needAuth, function(req, res, next) {
	var email = req.body.email || "";
	var result = {};
	if(!email || !((new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g)).test(email))){
		result.status = false;
		result.msg = "이메일 형식이 올바르지 않습니다.";
		result.focus = "email";
	}else{
		var email_from = req.user.email;
		var mailData = {
			host: req.get('host'),
			email: email,
			email_from: email_from,
			survey_id: req.body.survey_id
		};
		nodemailer(mailData,"emailInvite");
		result.status = true;
		result.msg = "초대 이메일이 전송되었습니다.";
	}
	res.json(result);
});

/* 설문 작성 페이지 */
router.get('/write', needAuth, function(req, res, next) {
	res.render('survey/write');
});

/* 설문 수정 페이지 */
router.get('/edit/id/:id/page/:page', needAuth, function(req, res, next) {
	getSurvey(req.params.id, function(err, survey){
		if(err){
			return next(err);
		}else{
			res.render('survey/write', {survey: survey, fromPage: req.params.page});
		}
	});
});

/* 설문 응답 완료 페이지 */
router.get('/response', function(req, res, next) {
	res.render('survey/response', {deleteNav: true});
});

/* 설문 응답(수집) 페이지 */
router.get('/response/id/:id', function(req, res, next) {
	getSurvey(req.params.id, function(err, survey){
		if(err){
			return next(err);
		}else{
			var isExpire = false;
			if(survey.useDate){
				// 설문기간 확인
				var currentTimestamp = moment().valueOf();
				var startTimestamp = moment(survey.startDate).valueOf();
				var endTimestamp = moment(survey.endDate).valueOf();
				isExpire = (startTimestamp <= currentTimestamp && currentTimestamp <= endTimestamp) ? false : true;
			}

			res.render('survey/response', {survey: survey, deleteNav: true, isExpire: isExpire});
		}
	});
});

/* 설문 작성 완료(db insert) */
router.post('/writeAction', needAuth, function(req, res, next) {
	var userid = req.user.id;
	req.body.userID = userid;
	saveSurvey(req.body, function(err) {
		if(err){
			return next(err);
		}else{
			req.flash('success', '설문이 등록 되었습니다.');
			res.redirect('/survey/list/page/1');
		}
    });
});

/* 설문 수정 완료(db update) */
router.put('/editAction/id/:id', function(req, res, next) {
	req.body.survey_id = req.params.id;
	req.body.user_id = req.user.id;
	editSurvey(req.body, function() {
		req.flash('success', '설문이 수정 되었습니다.');
		res.redirect('/survey/list/page/'+req.body.fromPage);
    });
});

// 설문 삭제
router.delete('/:id', needAuth, function(req, res, next) {
	Survey.findOneAndRemove({_id: req.params.id, userID: req.user.id}, function(err) {
	    if (err) {
	    	return next(err);
	    }else{
	    	Response.remove({survey_id: {$in: req.params.id}}, function(err) {
	    		if(err){
	    			return next(err);
	    		}else{
	    			req.flash('success', '설문이 삭제되었습니다.');
	    		    res.redirect('/survey/list/page/1');
	    		}
	    	});
	    }
	});
});

/* 설문 응답 완료(db insert) */
router.post('/responseAction/id/:id', needAuth, function(req, res, next) {
	req.body.survey_id = req.params.id;
	saveResponse(req.body, function(err, response){
		if(err){
			return next(err);
		}else{
			res.redirect('/survey/response');
		}
	});
});

/* 설문 결과 페이지 */
router.get('/result/id/:id/page/:page', needAuth, function(req, res, next) {
	getSurvey(req.params.id, function(err, survey){
		if(err){
			return next(err);
		}else if(survey.userID != req.user.id){
			// user가 다른 비정상 접근
			return next(err);
		}else{
			getResponse(survey, function(err, response){
				if(err){
					return next(err);
				}else{
					res.render('survey/result', {survey: survey, response: response, fromPage: req.params.page});
				}
			});
		}
	});
});

// DB SELECT SURVEY
var getSurvey = function(id, callback){
	Survey.findById(id, function(err, survey) {
		if(err){
			callback(err, null);
		}else{
			callback(null, survey);
		}
	});
};

//DB INSERT SURVEY
var saveSurvey = function(data, callback){
	var surveyData = inputDataOrganization(data);
	var newSurvey = new Survey({
		title: surveyData.title,
		explain: surveyData.explain,
		useDate: surveyData.useDate,
		startDate: surveyData.startDate,
		endDate: surveyData.endDate,
		question: surveyData.question,
		userID: data.userID
	});
	
	newSurvey.save(function(err, survey) {
		if(err){
			console.log(err);
			callback(err);
		}else{
			callback(null);
	    }
	});
};

//DB UPDATE SURVEY
var editSurvey = function(data, callback){
	var surveyData = inputDataOrganization(data);
	Survey.findOne({_id:data.survey_id, userID:data.user_id}, function(err, survey) {
		if(err){
			callback(err, null);
		}else if(!survey){
			// 설문이 없을 경우 에러
			callback(err, null);
		}else{
			survey.title = surveyData.title;
			survey.explain = surveyData.explain;
			survey.useDate = surveyData.useDate;
			survey.startDate = surveyData.startDate;
			survey.endDate = surveyData.endDate;
			survey.question = surveyData.question;
			survey.updateVerision++;
			
			survey.save(function(err){
				if(err){
					callback(err, null);
				}else{
					callback(null, survey);
				}
			});
		}
	});
};

//DB SELECT RESPONSE
var getResponse = function(survey, callback){
	Response.find({survey_id: survey.id, survey_version: survey.updateVersion}, function(err, response) {
		if(err){
			callback(err, null);
		}else{
			callback(null, response);
		}
	});
};

//DB INSERT RESPONSE
var saveResponse = function(data, callback){
	getSurvey(data.survey_id, function(err, survey){
		if(err){
			callback(err, null);
		}else{
			var answerArray = [];
			var answer = {};
			
			for(var i=0; i<survey.question.length; i++){
				answer = {};
				if(Array.isArray(data["question"+(i+1)+"_answer"])){
					answer.master = data["question"+(i+1)+"_answer"][0];
					answer.slave = data["question"+(i+1)+"_answer"][1];
				}else{
					answer.master = data["question"+(i+1)+"_answer"];
				}
				answerArray.push(answer);
			}
			
			var newResponse = new Response({
				survey_id: survey.id,
				survey_version: survey.updateVersion,
				answer: answerArray
			});
			
			newResponse.save(function(err, response) {
				if(err){
					console.log(err);
					callback(err);
				}else{
					callback(null);
			    }
			});
		}
	});
};

// 설문작성, 편집시 DB작업전 정리
function inputDataOrganization(data){
	var rtnData = {};
	
	rtnData.title = data.title;
	rtnData.explain = data.explain;
	rtnData.useDate = (typeof data.useDate != 'undefined') ? true : false;
	rtnData.startDate = rtnData.useDate ? data.startDate : null;
	rtnData.endDate = rtnData.useDate ? data.endDate : null;
	
	var questionArray = [];
	var multiple;
	var question;
	var multipleData;
	var multipleArray;
	if(Array.isArray(data.questionIdx)){
		for(var i in data.questionIdx) {
			question = {};
			question.idx = data.questionIdx[i];
			question.name = data.questionName[i];
			question.kind = data.questionKind[i];
			question.indispensable = data.questionIndispensable[i];
			// 객관식
			if(question.kind == 1){
				multipleData = data["question"+question.idx+"_multiple"];
				multipleArray = [];
				if(Array.isArray(multipleData)){
					for(var j in multipleData) {
						multiple = {};
						multiple.kind = 0;	// 선택지
						multiple.name = multipleData[j];
						multipleArray.push(multiple);
					}
				}else{
					multiple = {};
					multiple.kind = 0;	// 선택지
					multiple.name = multipleData;
					multipleArray.push(multiple);
				}
				
				// '기타' 항목 사용시
				if(typeof data["question"+question.idx+"_multiple_etc"] != 'undefined'){
					multipleArray.push({kind:1, name:"기타"});
				}
				
				question.multiple = multipleArray;
			}
			questionArray.push(question);
		}
	}else{
		question = {};
		question.idx = data.questionIdx;
		question.name = data.questionName;
		question.kind = data.questionKind;
		question.indispensable = data.questionIndispensable;
		
		// 객관식
		if(question.kind == 1){
			multipleData = data["question"+question.idx+"_multiple"];
			multipleArray = [];
			if(Array.isArray(multipleData)){
				for(var k in multipleData) {
					multiple = {};
					multiple.kind = 0;
					multiple.name = multipleData[k];
					multipleArray.push(multiple);
				}
			}else{
				multiple = {};
				multiple.kind = 0;
				multiple.name = multipleData;
				multipleArray.push(multiple);
			}
			
			// '기타' 항목 사용시
			if(typeof data["question"+question.idx+"_multiple_etc"] != 'undefined'){
				multipleArray.push({kind:1, name:"기타"});
			}
			
			question.multiple = multipleArray;
			
		}
		questionArray.push(question);
	}
	
	rtnData.question = questionArray;
	
	return rtnData;
}

module.exports = router;