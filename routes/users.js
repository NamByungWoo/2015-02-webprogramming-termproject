var express = require('express');
var router = express.Router();
var nodemailer = require('../config/nodemailer');
var User = require('../models/User');

function needAuth(req, res, next) {
	if(req.isAuthenticated()){
		next();
	}else{
		req.flash('danger', '로그인이 필요합니다.');
		res.redirect('/');
	}
}

function validateForm(data){
	var result = {};
	var name = data.name || "";
	var email = data.email || "";
	var password = data.password || "";
	var password_confirmation = data.password_confirmation || "";
	name = name.trim();
	email = email.trim();
	password = password.trim();
	password_confirmation = password_confirmation.trim();
	
	if(!email){
		result.status = false;
		result.msg = "이메일을 입력해주세요.";
		result.focus = "email";
	}else if(!((new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g)).test(email))){
		result.status = false;
		result.msg = "이메일 형식이 올바르지 않습니다.";
		result.focus = "email";
	}else if(!name){
		result.status = false;
		result.msg = "이름을 입력해주세요.";
		result.focus = "name";
	}else if(!password){
		result.status = false;
		result.msg = "비밀번호를 입력해주세요.";
		result.focus = "password";
	}else if(!((new RegExp(/^[A-za-z0-9]{6,12}$/g)).test(password))){
		result.status = false;
		result.msg = "비밀번호는 6~12자 영문 대 소문자, 숫자를 사용하세요.";
		result.focus = "password";
	}else if(!password_confirmation){
		result.status = false;
		result.msg = "비밀번호 확인을 입력해주세요.";
		result.focus = "password_confirmation";
	}else if(password != password_confirmation){
		result.status = false;
		result.msg = "비밀번호가 일치하지 않습니다.";
		result.focus = "password_confirmation";
	}else{
		result.status = true;
	}
	return result;
}

/* 회원가입 validate ajax */
router.post('/signupAjax', function(req, res, next) {
	User.count({email:req.body.email}, function(err, count){
		if(count>0){
			res.json({
				status : false,
				msg : "동일한 이메일 주소가 이미 존재합니다.",
				focus : "email"
			});
		}else{
			res.json(validateForm(req.body));
		}
	});
});

/* 회원가입 로직(이메일 승인 대기) */
router.post('/signup', function(req, res, next) {
	User.count({email:req.body.email}, function(err, count){
		if(count>0 || !(validateForm(req.body)).status){
			req.flash('danger', '잘못된 접근 입니다.');
			res.redirect('/');
		}else{
			var newUser = new User({
				name: req.body.name,
				email: req.body.email,
			});
			newUser.password = newUser.generateHash(req.body.password);
			newUser.save(function(err, user) {
				if(err){
					next(err);
				}else{
					req.body.host = req.get('host');
					req.body.userID = user.id;
					nodemailer(req.body,"signupVerify");
					req.flash('success', '가입이 완료되었습니다. 로그인 해주세요.');
					res.redirect('/');
			    }
			}); 	
		}
	});
});

/* 회원가입 이메일 승인 */
router.get('/signupVerify/code/:code/email/:email', function(req, res, next) {
	User.findOneAndUpdate({_id: req.params.code, email: req.params.email, signupVerify:false}, {signupVerify: true}, function(err, post) {
		if (err) {
			return next(err);
		}
		req.flash('success', '인증이 완료되었습니다. 로그인 해주세요.');
		res.redirect('/');
	});
});

/* 마이페이지 */
router.get('/mypage', needAuth, function(req, res, next) {
	getUser(req.user.id, function(err, user){
		if(err){
			return next(err);
		}else{
			res.render('users/mypage', {user: user});
		}
	});
});

/* 마이페이지 수정 요청시 validate ajax */
router.post('/mypageAjax', needAuth, function(req, res, next) {
	var result = {};
	var name = req.body.name || "";
	var password = req.body.password || "";
	var password_confirmation = req.body.password_confirmation || "";
	name = name.trim();
	password = password.trim();
	password_confirmation = password_confirmation.trim();
	
	if(!name){
		result.status = false;
		result.msg = "이름을 입력해주세요.";
		result.focus = "name";
	}else if(!password){
		result.status = false;
		result.msg = "비밀번호를 입력해주세요.";
		result.focus = "password";
	}else if(!((new RegExp(/^[A-za-z0-9]{6,12}$/g)).test(password))){
		result.status = false;
		result.msg = "비밀번호는 6~12자 영문 대 소문자, 숫자를 사용하세요.";
		result.focus = "password";
	}else if(!password_confirmation){
		result.status = false;
		result.msg = "비밀번호 확인을 입력해주세요.";
		result.focus = "password_confirmation";
	}else if(password != password_confirmation){
		result.status = false;
		result.msg = "비밀번호가 일치하지 않습니다.";
		result.focus = "password_confirmation";
	}else{
		result.status = true;
	}

	res.json(result);
});

/* 회원 수정 완료(db update) */
router.put('/editAction', needAuth, function(req, res, next) {
	req.body.user_id = req.user.id;
	editUser(req.body, function() {
		req.flash('success', '회원정보가 수정 되었습니다.');
		res.redirect('/users/mypage');
    });
});

// 회원 - 탈퇴 신청
router.delete('/', needAuth, function(req, res, next) {
	deleteUser(req.user.id, function(err){
		if(err){
			return next(err);
		}else{
			req.flash('success', '탈퇴 되었습니다.');
			res.redirect('/');
		}
	});
});

// 비밀번호 재설정 이메일 전송 요청시 validate ajax
router.post('/forgotPasswordAjax', function(req, res, next) {
	User.count({email:req.body.email}, function(err, count){
		var result = {};
		var email = req.body.email || "";
		email = email.trim();
		
		if(!email){
			result.status = false;
			result.msg = "이메일을 입력해주세요.";
			result.focus = "email";
		}else if(!((new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g)).test(email))){
			result.status = false;
			result.msg = "이메일 형식이 올바르지 않습니다.";
			result.focus = "email";
		}else if(count==0){
			result.status = false;
			result.msg = "'" + email + "' 계정이 존재하지 않습니다.";
			result.focus = "email";
		}else{
			// 존재할경우 true
			result.status = true;
		}
		res.json(result);
	});
});

/* 비밀번호 재설정 이메일 전송 로직 */
router.post('/forgotPasswordEmailSend', function(req, res, next) {
	User.findOne({email:req.body.email}, function(err, user) {
		if(err){
			return next(err);
		}else if(!user){
			// 회원이 없을 경우 에러
			return next(err);
		}else{
			var mailData = {
				user_id : user.id,
				email : user.email,
				name : user.name,
				host : req.get('host')
			}
			
			nodemailer(mailData,"forgotPassword");
			
			req.flash('success', user.email+'로 비밀번호 재설정을 위한 링크를 전송했습니다.');
			res.redirect('/');
		}
	});
});

/* 비밀번호 재설정 페이지 */
router.get('/resetPassword/id/:id', function(req, res, next) {
	User.count({_id:req.params.id}, function(err, count){		
		if(err){
			return next(err);
		}else if(count==0){
			return next(err);
		}else{
			res.render('users/resetPassword', {user_id:req.params.id});
		}
	});
});

/* 비밀번호 재설정 요청시 validate ajax */
router.post('/resetPasswordAjax', function(req, res, next) {
	var result = {};
	var password = req.body.password || "";
	var password_confirmation = req.body.password_confirmation || "";
	password = password.trim();
	password_confirmation = password_confirmation.trim();
	
	if(!password){
		result.status = false;
		result.msg = "새 비밀번호를 입력해주세요.";
		result.focus = "password";
	}else if(!((new RegExp(/^[A-za-z0-9]{6,12}$/g)).test(password))){
		result.status = false;
		result.msg = "비밀번호는 6~12자 영문 대 소문자, 숫자를 사용하세요.";
		result.focus = "password";
	}else if(!password_confirmation){
		result.status = false;
		result.msg = "비밀번호 확인을 입력해주세요.";
		result.focus = "password_confirmation";
	}else if(password != password_confirmation){
		result.status = false;
		result.msg = "비밀번호가 일치하지 않습니다.";
		result.focus = "password_confirmation";
	}else{
		result.status = true;
	}

	res.json(result);
});

/* 비밀번호 재설정 로직 */
router.put('/resetPasswordAction', function(req, res, next) {
	console.log(req.body);
	User.findById(req.body.user_id, function(err, user) {
		if(err){
			return next(err);
		}else if(!user){
			// 회원이 없을 경우 에러
			return next(err);
		}else{
			user.password = new User({}).generateHash(req.body.password);
			
			user.save(function(err){
				if(err){
					return next(err);
				}else{
					req.flash('success', '비밀번호가 재설정 되었습니다. 로그인 해주세요.');
					res.redirect('/');
				}
			});
		}
	});
});

//DB SELECT USER
var getUser = function(id, callback){
	User.findById(id, function(err, user) {
		if(err){
			callback(err, null);
		}else{
			callback(null, user);
		}
	});
};

//DB UPDATE USER
var editUser = function(data, callback){
	User.findById(data.user_id, function(err, user) {
		if(err){
			callback(err, null);
		}else if(!user){
			// 회원이 없을 경우 에러
			callback(err, null);
		}else{
			user.name = data.name;
			user.password = new User({}).generateHash(data.password);
			
			user.save(function(err){
				if(err){
					callback(err, null);
				}else{
					callback(null, user);
				}
			});
		}
	});
};

//DB DELETE USER
var deleteUser = function(id, callback){
	User.findOneAndRemove({_id: id}, function(err) {
	    if (err) {
	    	callback(err);
	    }else{
	    	callback(null);
	    }
	});
};

module.exports = router;
