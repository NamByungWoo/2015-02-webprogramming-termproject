var express = require('express');
var router = express.Router();

/* 메인페이지 */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'Star Survey - 온라인 설문 조사 서비스' });
});

module.exports = router;