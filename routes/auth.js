module.exports = function(app, passport) {
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/', // redirect to the secure profile section
		failureRedirect : '/', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	app.get('/auth/facebook',passport.authenticate('facebook', { scope : 'email' }));

	app.get('/auth/facebook/callback',
		passport.authenticate('facebook', {
			failureRedirect : '/#',
			failureFlash : '페이스북 로그인이 실패하였습니다.' // allow flash messages
		}),
		function(req, res, next) {
			if(typeof req.user == 'undefined'){
				req.flash('danger', '페이스북 이메일 인증을 먼저 해주세요');
			}else{
				req.flash('success', '로그인되었습니다.');
			}
			res.redirect('/#');
		}
	);

	app.get('/logout', function(req, res) {
		req.logout();
		req.flash('success', '로그아웃 되었습니다.');
		res.redirect('/');
	});
};
