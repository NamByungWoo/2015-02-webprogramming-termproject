var express = require('express');
var router = express.Router();
var pagination = require('../config/pagination');
var User = require('../models/User');
var Survey = require('../models/Survey');
var user_r = require('../routes/users');

function needAdmin(req, res, next) {
	if(req.isAuthenticated()){
		if(req.user.isAdmin){
			next();
		}else{
			req.flash('danger', '잘못된 접근 입니다.');
			res.redirect('/');
		}
	}else{
		req.flash('danger', '로그인이 필요합니다.');
		res.redirect('/');
	}
}

function validateAdduser(data){
	var result = {};
	var name = data.name || "";
	var email = data.email || "";
	var password = data.password || "";
	var password_confirmation = data.password_confirmation || "";
	var signupVerify = data.signupVerify || "";
	name = name.trim();
	email = email.trim();
	password = password.trim();
	password_confirmation = password_confirmation.trim();
	signupVerify = signupVerify.trim();
	
	if(!email){
		result.status = false;
		result.msg = "이메일을 입력해주세요.";
		result.focus = "email";
	}else if(!((new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g)).test(email))){
		result.status = false;
		result.msg = "이메일 형식이 올바르지 않습니다.";
		result.focus = "email";
	}else if(!name){
		result.status = false;
		result.msg = "이름을 입력해주세요.";
		result.focus = "name";
	}else if(!password){
		result.status = false;
		result.msg = "비밀번호를 입력해주세요.";
		result.focus = "password";
	}else if(!((new RegExp(/^[A-za-z0-9]{6,12}$/g)).test(password))){
		result.status = false;
		result.msg = "비밀번호는 6~12자 영문 대 소문자, 숫자를 사용하세요.";
		result.focus = "password";
	}else if(!password_confirmation){
		result.status = false;
		result.msg = "비밀번호 확인을 입력해주세요.";
		result.focus = "password_confirmation";
	}else if(password != password_confirmation){
		result.status = false;
		result.msg = "비밀번호가 일치하지 않습니다.";
		result.focus = "password_confirmation";
	}else if(!signupVerify){
		result.status = false;
		result.msg = "이메일 인증 여부를 선택해주세요.";
		result.focus = "signupVerify_true";
	}else{
		result.status = true;
	}
	return result;
}

// 페이지정보가 없을 때 무조건 1페이지로
router.get('/userlist', function(req, res, next) {
	res.redirect('/admin/userlist/page/1');
});

/* 회원 목록 페이지 */
router.get('/userlist/page/:page', needAdmin, function(req, res, next) {
	User.count({isAdmin: false}, function(err, count){
		// 페이징을 위한 db select option정의
		var option = {
			listURL: "/admin/userlist",
			limit: 10,	// 한번에 가져올 최대 데이터 수
			skip: (req.params.page-1)*10,	// 몇개의 데이터를 건너 뛰고 select할지...(현재 page에따라 변함)
			pagingGroupNum: 4,
			sort: {createdAt:-1}	// 등록일을 기준으로 내림차순 정렬
		};
		
		// 앞서 입력된 정보를 가지고 현재페이지에 나타낼 실제 리스트를 select
		User.find({isAdmin: false}, {}, option, function(err, users){
			if(err){
				return next(err);
			}else{
				res.render('admin/userlist', {users: users, pagination: pagination(req.params.page, count, option)});
			}
		});
	});
});

/* 회원추가 validate ajax */
router.post('/adduserAjax', needAdmin, function(req, res, next) {
	User.count({email:req.body.email}, function(err, count){
		if(count>0){
			res.json({
				status : false,
				msg : "동일한 이메일 주소가 이미 존재합니다.",
				focus : "email"
			});
		}else{
			res.json(validateAdduser(req.body));
		}
	});
});

/* 회원추가 로직 */
router.post('/adduser', needAdmin, function(req, res, next) {
	User.count({email:req.body.email}, function(err, count){
		if(count>0 || !(validateAdduser(req.body)).status){
			req.flash('danger', '잘못된 접근 입니다.');
			res.redirect('/');
		}else{
			var newUser = new User({
				name: req.body.name,
				email: req.body.email,
				signupVerify: req.body.signupVerify
			});
			newUser.password = newUser.generateHash(req.body.password);
			newUser.save(function(err, user) {
				if(err){
					return next(err);
				}else{
					req.flash('success', '회원이 추가 되었습니다.');
					res.redirect('/admin/userlist/page/1');
			    }
			}); 	
		}
	});
});

//관리자 - 회원 삭제
router.delete('/userDelete/:id', needAdmin, function(req, res, next) {
	User.findOneAndRemove({_id: req.params.id}, function(err) {
	    if (err) {
	    	return next(err);
	    }else{
	    	if(err){
				return next(err);
			}else{
				req.flash('success', '회원이 삭제되었습니다.');
		    	res.redirect('/admin/userlist/page/1');
			}
	    }
	});
});

/* 회원수정 페이지 */
router.get('/useredit/id/:id/page/:page', needAdmin, function(req, res, next) {
	User.findById(req.params.id, function(err, user) {
		if(err){
			return next(err);
		}else{
			res.render('admin/useredit', {user: user, fromPage: req.params.page});
		}
	});
});

/* 회원 수정 요청시 validate ajax */
router.post('/usereditAjax', needAdmin, function(req, res, next) {
	var result = {};
	var name = req.body.name || "";
	var email = req.body.email || "";
	var password = req.body.password || "";
	var password_confirmation = req.body.password_confirmation || "";
	name = name.trim();
	email = email.trim();
	password = password.trim();
	password_confirmation = password_confirmation.trim();

	if(!email){
		result.status = false;
		result.msg = "이메일을 입력해주세요.";
		result.focus = "email";
	}else if(!((new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g)).test(email))){
		result.status = false;
		result.msg = "이메일 형식이 올바르지 않습니다.";
		result.focus = "email";
	}else if(!name){
		result.status = false;
		result.msg = "이름을 입력해주세요.";
		result.focus = "name";
	}else if(!password){
		result.status = false;
		result.msg = "비밀번호를 입력해주세요.";
		result.focus = "password";
	}else if(!((new RegExp(/^[A-za-z0-9]{6,12}$/g)).test(password))){
		result.status = false;
		result.msg = "비밀번호는 6~12자 영문 대 소문자, 숫자를 사용하세요.";
		result.focus = "password";
	}else if(!password_confirmation){
		result.status = false;
		result.msg = "비밀번호 확인을 입력해주세요.";
		result.focus = "password_confirmation";
	}else if(password != password_confirmation){
		result.status = false;
		result.msg = "비밀번호가 일치하지 않습니다.";
		result.focus = "password_confirmation";
	}else{
		result.status = true;
	}
	
	res.json(result);
});

/* 회원 수정 완료(db update) */
router.put('/usereditAction', needAdmin, function(req, res, next) {
	User.findById(req.body.user_id, function(err, user) {
		if(err){
			return next(err);
		}else if(!user){
			// 회원이 없을 경우 에러
			return next(err);
		}else{
			user.name = req.body.name;
			user.password = new User({}).generateHash(req.body.password);
			
			user.save(function(err){
				if(err){
					return next(err);
				}else{
					req.flash('success', '회원정보가 수정 되었습니다.');
					res.redirect('/admin/userlist/page/'+req.body.fromPage);
				}
			});
		}
	});
});

module.exports = router;